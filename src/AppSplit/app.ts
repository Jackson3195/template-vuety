import Vue from "vue";
import { Component, Data, Lifecycle } from "vuety";
import Hello from "./HelloSplit/hello";

import "./app.scss";

@Component({
  name: "MainApp",
  template: require("./app.html"),
  components: {
    "Hello": Hello
  }
})
export default class App extends Vue {
  @Data protected helloMsg: string;
  @Data protected propMessage: string;

  @Lifecycle protected created() {
    this.propMessage = "Prop message from App Component";
  }
  @Lifecycle protected mounted() {
    this.helloMsg = "hello world! - App Component";
  }
}
