import Vue from "vue";
import { Component, Data, Lifecycle, Prop } from "vuety";

import "./hello.scss";

@Component({
  template: require("./hello.html"),
  name: "Hello"
})
export default class Hello extends Vue {
  @Data protected helloMsg: string;

  @Prop({
    default: "Default Value"
  }) protected propMessage: string;

  @Lifecycle protected mounted() {
    this.helloMsg = "This message is generated when mounted - Hello Component!";
  }
}
