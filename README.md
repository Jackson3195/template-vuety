# Alpha Build - VueJS + TypeScript + Vuety (Minimal)

> A template to incorporate TypeScript into Vue.JS using the [Vuety](https://www.npmjs.com/package/vuety) package.

## Why Vuety?
> [Vue-class-component](https://github.com/vuejs/vue-class-component) & [Vue-property-decorator](https://github.com/kaorun343/vue-property-decorator) are both required in order to build a standard component. [Vuety](https://www.npmjs.com/package/vuety) seems combine both into one package.


## Build using single file or multi-file components.
> See the './src' file for examples on both ('./src/AppSplit/' for multi-file component example).
### Why the option?
#### Single File:
> Single file combine HTML + CSS + JS/TS but unfortunetly 'TSlint' does not support '*.vue' extentions, thus you lose helpful hints, type safety checks are less maintainable but fimilar as the template is similar to that of the standard '*.vue' files.
#### Multi File:
> Multi file components split the HTML, CSS and JS/TS into individual files, these are then 'import'ed or 'require'ed into the
'.ts' file, the biggest benefit is TSLint supporting '*.ts' extentions, thus type safety checks can be better maintained. This personally works great on [VSCode](https://code.visualstudio.com/).

### TS Lint
> Comes with the template by default and can be configured in the 'tslint.json' file.

## Current known issues with the template:
1. Webpack does not reload webpage on successful compile, have to manually refresh.
2. Template needs trimming from unused modules and files, webpack needs cleaning up.
```
- Removed ESLint.
```
3. 'main.ts' file unable to find '*.vue' extentions (Delcarations found in 'sfc.d.ts').

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## Basic recipe...
1. Created a standard VueJS project using VueCLI.
2. Incorporated [vue-class-component](https://github.com/vuejs/vue-class-component).
3. Added TypeScript support to 'vue-class-component'.
4. Switched from 'vue-class-component' to 'vuety'.
5. Added necessary loaders to support multi-file development + TSLint.
